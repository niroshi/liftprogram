﻿/* The class represents a request for the elevator to move.
 * The request could be either a request to bring the elevator to the floor the user is currently in or 
 * it could be request to travel to a particular floor upon getting into the elevator.
 */
namespace ElevatorProgram
{
    public enum RequestedType { DirectionButton, FloorButton, None }

    public class ElevatorRequest
    {
        public int requestedFloor { get; set; }
        public ElevatorDirection direction { get; set; }
        public RequestedType requestType { get; set; }


        public ElevatorRequest(int requestedFloor, RequestedType requestType, ElevatorDirection direction)
        {
            this.requestedFloor = requestedFloor;
            this.direction = direction;
            this.requestType = requestType;
        }

    }
}
