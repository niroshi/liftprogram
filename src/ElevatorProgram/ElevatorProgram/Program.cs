using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorProgram
{


    class Program : Elevator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("        Running Scenario 1");
            ElevatorTestProgram1 et1 = new ElevatorTestProgram1();
            et1.RunScenario();

            Console.WriteLine("");
            Console.WriteLine("**************************");
            Console.WriteLine("        Running Scenario 2");
            ElevatorTestProgram2 et2 = new ElevatorTestProgram2();
            et2.RunScenario();

            Console.WriteLine("");
            Console.WriteLine("**************************");
            Console.WriteLine("        Running Scenario 3");
            ElevatorTestProgram3 et3 = new ElevatorTestProgram3();
            et3.RunScenario();

            Console.WriteLine("");
            Console.WriteLine("**************************");
            Console.WriteLine("        Running Scenario 4");
            ElevatorTestProgram4 et4 = new ElevatorTestProgram4();
            et4.RunScenario();

            Console.ReadKey();
        }
    }
}
