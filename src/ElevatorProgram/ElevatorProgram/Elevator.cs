﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorProgram
{
    public enum ElevatorDirection
    {
        Down = 0,
        Up = 1
    }

    public class Elevator
    {
        public int currentFloorNo { get; set; }
        public ElevatorDirection currentDirection { get; set; }

        //tracks the current state the elevator is in
        public ElevatorState currentState { get; set; }

        public MovingUpElevatorState movingUpElevatorState { get; set; }
        public MovingDownElevatorState MovingDownElevatorState { get; set; }
        public IdleElevatorState idleElevatorState { get; set; }

        public List<ElevatorRequest> elevatorRequests = new List<ElevatorRequest>();
        public int destinationFloor;
        public List<int> elevatorHistory = new List<int>();

        public Elevator()
        {
            this.movingUpElevatorState = new MovingUpElevatorState(this);
            this.MovingDownElevatorState = new MovingDownElevatorState(this);
            this.idleElevatorState = new IdleElevatorState(this);
        }


        //A callback method that would be invoked by the relevante State objects
        public virtual void OnFloorReached(int floor)
        {
           
        }

        
        public void OpenDoor(int floor)
        {
            Console.WriteLine("Opening Door....");
            elevatorHistory.Add(floor);
        }


        // This method deals with the event where a user has requested the elevator
        public void PressedDirectionButton(Elevator elevator, int from, ElevatorDirection direction)
        {
            var requestObject = new ElevatorRequest(from, RequestedType.DirectionButton, direction);
            elevatorRequests.Add(requestObject);

            elevator.currentState.ProcessRequest();

        }


        // The method deals with the event where the user has selected a floor, after getting into the lift
        public void PressedFloorButton(Elevator elevator, int to)
        {
            var requestObject = new ElevatorRequest(to, RequestedType.FloorButton, this.currentDirection);
            elevatorRequests.Add(requestObject);

            elevator.currentState.ProcessRequest();
        }
    }
}
