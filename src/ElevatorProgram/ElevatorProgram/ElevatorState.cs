﻿using System;
using System.Linq;


namespace ElevatorProgram
{
    // The abstract class that represents the state the elevator is in
    public abstract class ElevatorState
    {
        public string stateDescription { get; set; }
        public Elevator elevator;

        public abstract void ProcessRequest();
    }

    // Represents the state where the elevator is moving up
    public class MovingUpElevatorState : ElevatorState
    {
        public MovingUpElevatorState(Elevator elevator)
        {
            this.elevator = elevator;
            stateDescription = "Moving Up";
        }

        public override void ProcessRequest()
        {
            if (elevator.destinationFloor > elevator.currentFloorNo)
            {
                while (elevator.destinationFloor != elevator.currentFloorNo)
                {
                    elevator.currentFloorNo++;
                    Console.WriteLine("Now at Level " + elevator.currentFloorNo);

                }

                //elevator has reached the desired floor.Therefore it would be idle
                elevator.currentState = elevator.idleElevatorState;

            }
            else if (elevator.destinationFloor == elevator.currentFloorNo)
            {
                elevator.currentState = elevator.idleElevatorState;
            }

            elevator.OpenDoor(elevator.currentFloorNo);

            // check whether a user is waiting in the current floor
            var hasAPassenger = elevator.elevatorRequests.Find(x => x.requestedFloor == elevator.currentFloorNo && x.requestType == RequestedType.DirectionButton);

            // remove the processed request
            elevator.elevatorRequests.Remove(elevator.elevatorRequests.Find(x => x.requestedFloor == elevator.currentFloorNo));
            if (hasAPassenger != null)
            {
                elevator.OnFloorReached(elevator.currentFloorNo);
            }
        }
    }


    //represents the state where the elevator is moving down
    public class MovingDownElevatorState : ElevatorState
    {
        public MovingDownElevatorState(Elevator elevator)
        {
            this.elevator = elevator;
            stateDescription = "Moving Down";
        }

        public override void ProcessRequest()
        {
            if (elevator.destinationFloor < elevator.currentFloorNo)
            {
                while (elevator.destinationFloor != elevator.currentFloorNo)
                {
                    elevator.currentFloorNo--;
                    Console.WriteLine("Now at Level " + elevator.currentFloorNo);

                }

                //elevator has reached the desired floor.Therefore it would be idle
                elevator.currentState = elevator.idleElevatorState;

            }
            else if (elevator.destinationFloor == elevator.currentFloorNo)
            {
                elevator.currentState = elevator.idleElevatorState;
            }

            elevator.OpenDoor(elevator.currentFloorNo);

            // remove the processed request
            var hasAPassenger = elevator.elevatorRequests.Find(x => x.requestedFloor == elevator.currentFloorNo && x.requestType == RequestedType.DirectionButton);

            //remove the processed request
            elevator.elevatorRequests.Remove(elevator.elevatorRequests.Find(x => x.requestedFloor == elevator.currentFloorNo));
            if (hasAPassenger != null)
            {
                elevator.OnFloorReached(elevator.currentFloorNo);

            }
        }
    }


    //represents a state where the elevator is neither moving up or down
    public class IdleElevatorState : ElevatorState
    {
        public IdleElevatorState(Elevator elevator)
        {
            this.elevator = elevator;
            stateDescription = "Stopped";
        }

        public override void ProcessRequest()
        {
            foreach (var req in elevator.elevatorRequests.ToList())
            {
                // process DirectionButtonRequests
                if (req.requestType == RequestedType.DirectionButton)
                {
                    Console.WriteLine("Pressed " + req.direction + " from " + req.requestedFloor);
                    if (elevator.currentFloorNo != req.requestedFloor)
                    {
                        //If current Floor is below requested changing the elevator state to MovingUpState
                        if (elevator.currentFloorNo < req.requestedFloor)
                        {
                            elevator.currentState = elevator.movingUpElevatorState;
                        }
                        //If current Floor is above requested changing the elevator state to MovingDownState
                        else if (elevator.currentFloorNo > req.requestedFloor)
                        {
                            elevator.currentState = elevator.MovingDownElevatorState;
                        }

                        //Assign the requested floor as the destination floor and start processing the request
                        elevator.destinationFloor = req.requestedFloor;
                        elevator.currentState.ProcessRequest();

                    }
                    else
                    {
                        //If current floor is already the rerquested floor, open the door and remove from requestList
                        elevator.OpenDoor(req.requestedFloor);
                        elevator.elevatorRequests.Remove(req);
                        elevator.OnFloorReached(req.requestedFloor);
                        break;

                    }
                }
                // process FloorButtonRequests
                else if (req.requestType == RequestedType.FloorButton)
                {
                    if (elevator.currentFloorNo != req.requestedFloor)
                    {
                        //If requestedfloor is above currentFloor, change the state to MovingUpElevatorState
                        if (elevator.currentFloorNo < req.requestedFloor)
                        {
                            Console.WriteLine("Pressed floor button " + req.requestedFloor);
                            elevator.currentState = elevator.movingUpElevatorState;
                        }
                        //If requestedfloor is below currentFloor, change the state to MovingDownElevatorState
                        else if (elevator.currentFloorNo > req.requestedFloor)
                        {
                            Console.WriteLine("Pressed floor button " + req.requestedFloor);
                            elevator.currentState = elevator.MovingDownElevatorState;
                        }

                        elevator.destinationFloor = req.requestedFloor;
                        elevator.currentState.ProcessRequest();
                    }
                    //If requestedFloor is currentFloor, Open the door
                    else
                    {
                        elevator.OpenDoor(req.requestedFloor);
                        elevator.elevatorRequests.Remove(req);
                        elevator.OnFloorReached(req.requestedFloor);
                        break;

                    }
                }
            }


        }
    }
}
