﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorProgram
{
    //Test Class scenario => Passenger clicks Up from floor 0 to go to floor 5
    public class ElevatorTestProgram1 : Elevator
    {
        public List<int> expectedFloorList = new List<int>();

        //Overriden Method to add floor button press
        override public void OnFloorReached(int floor)
        {
            if (floor == 0)
            {
                this.PressedFloorButton(this, 5);
            }
        }

        public void RunScenario()
        {
            //Arrange
            ElevatorTestProgram1 elevator = new ElevatorTestProgram1();
            elevator.currentState = elevator.idleElevatorState;
            elevator.currentFloorNo = 0;
            elevator.currentDirection = ElevatorDirection.Up;
            elevator.elevatorHistory.Clear();

            //Act
            elevator.PressedDirectionButton(elevator, 0, ElevatorDirection.Up);

            //Assert
            expectedFloorList.Add(0);
            expectedFloorList.Add(5);

            try
            {
                CollectionAssert.AreEqual(expectedFloorList, elevator.elevatorHistory, "Test Failed : Actual Result is not same as Expected Result");
                Console.WriteLine(" ======== Test Case Passed ");
            }
            catch (Exception e)
            {
                Console.WriteLine(" ======== Test Case Failed : Actual Result is not same as Expected Result");
            }
        }
    }

    //Test Class scenario => Passenger1 clicks Down from floor 6 to go to floor 1 and 
    //                       Passenger2 clicks Down from floor 4 to go to floor 1
    public class ElevatorTestProgram2 : Elevator
    {
        public List<int> expectedFloorList = new List<int>();

        //Overriden Method to add floor button press
        override public void OnFloorReached(int floor)
        {
            if (floor == 6 || floor == 4)
            {
                this.PressedFloorButton(this, 1);
            }
        }

        
        public void RunScenario()
        {
            //Arrange
            ElevatorTestProgram2 elevator = new ElevatorTestProgram2();
            elevator.currentState = elevator.idleElevatorState;
            elevator.currentFloorNo = 0;
            elevator.currentDirection = ElevatorDirection.Down;

            //Act
            elevator.PressedDirectionButton(elevator, 6, ElevatorDirection.Down);
            elevator.PressedDirectionButton(elevator, 4, ElevatorDirection.Down);

            //Assert
            expectedFloorList.Add(6);
            expectedFloorList.Add(1);
            expectedFloorList.Add(4);
            expectedFloorList.Add(1);

            try
            {
                CollectionAssert.AreEqual(expectedFloorList, elevator.elevatorHistory, "Test Failed : Actual Result is not same as Expected Result");
                Console.WriteLine(" ======== Test Case Passed ");
            }
            catch (Exception e)
            {
                Console.WriteLine(" ======== Test Case Failed : Actual Result is not same as Expected Result");
            }
        }
    }

    //Test Class scenario => Passenger1 clicks Up from floor 2 to go to floor 6 and 
    //                       Passenger2 clicks Down from floor 4 to go to floor 0
    public class ElevatorTestProgram3 : Elevator
    {
        public List<int> expectedFloorList = new List<int>();

        //Overriden Method to add floor button press
        override public void OnFloorReached(int floor)
        {
            if (floor == 2)
            {
                this.PressedFloorButton(this, 6);
            }
            if (floor == 4)
            {
                this.PressedFloorButton(this, 0);
            }

        }

        public void RunScenario()
        {
            //Arrange
            ElevatorTestProgram3 elevator = new ElevatorTestProgram3();
            elevator.currentState = elevator.idleElevatorState;
            elevator.currentFloorNo = 0;
            elevator.currentDirection = ElevatorDirection.Up;

            //Act
            elevator.PressedDirectionButton(elevator, 2, ElevatorDirection.Up);
            elevator.PressedDirectionButton(elevator, 4, ElevatorDirection.Down);

            //Assert
            expectedFloorList.Add(2);
            expectedFloorList.Add(6);
            expectedFloorList.Add(4);
            expectedFloorList.Add(0);

            try
            {
                CollectionAssert.AreEqual(expectedFloorList, elevator.elevatorHistory, "Test Failed : Actual Result is not same as Expected Result");
                Console.WriteLine(" ======== Test Case Passed ");
            }
            catch(Exception e)
            {
                Console.WriteLine(" ======== Test Case Failed : Actual Result is not same as Expected Result");
            }
        }
    }


    //Test Class scenario => Passenger1 clicks Up from floor 0 to go to floor 5 and 
    //                       Passenger2 clicks Down from floor 4 to go to floor 0 and
    //                       Passenger3 clicks Down from floor 10 to go to floor 0 and
    public class ElevatorTestProgram4 : Elevator
    {
        public List<int> expectedFloorList = new List<int>();

        //Overriden Method to add floor button press
        override public void OnFloorReached(int floor)
        {
            if (floor == 0)
            {
                this.PressedFloorButton(this, 5);
            }
            else if (floor == 4)
            {
                this.PressedFloorButton(this, 0);
            }
            else if (floor == 10)
            {
                this.PressedFloorButton(this, 0);
            }

        }

        public void RunScenario()
        {
            //Arrange
            ElevatorTestProgram4 elevator = new ElevatorTestProgram4();
            elevator.currentState = elevator.idleElevatorState;
            elevator.currentFloorNo = 0;
            elevator.currentDirection = ElevatorDirection.Up;

            //Act
            elevator.PressedDirectionButton(elevator, 0, ElevatorDirection.Up);
            elevator.PressedDirectionButton(elevator, 4, ElevatorDirection.Down);
            elevator.PressedDirectionButton(elevator, 10, ElevatorDirection.Down);

            //Assert
            expectedFloorList.Add(0);
            expectedFloorList.Add(5);
            expectedFloorList.Add(4);
            expectedFloorList.Add(0);
            expectedFloorList.Add(10);
            expectedFloorList.Add(0);

            try
            {
                CollectionAssert.AreEqual(expectedFloorList, elevator.elevatorHistory, "Test Failed : Actual Result is not same as Expected Result");
                Console.WriteLine(" ======== Test Case Passed ");
            }
            catch (Exception e)
            {
                Console.WriteLine(" ======== Test Case Failed : Actual Result is not same as Expected Result");
            }
        }
    }



}
