# LiftProgram
## How to Run

Prerequisite
- Install Visual Studio 

Steps
- Download the src folder in the repository and open the solution in Visual Studio.
- Right click on the 'ElevatorProgram' Project and 'select Build' (or �trl, Shift & B)
- Run the program (F5)

Alternatively you can download the bin folder from the repositor and double click on the executable 'ElevatorProgram.exe'.

## Source code location - /src/ElevatorProgram/ElevatorProgram/

## Design consideration
The design in based on the State Design pattern. 
The Lift would respond to the requests by the user, based on the current state it is in.
![alternativetext](https://bitbucket.org/niroshi/liftprogram/raw/e4e0790c2703f4debb2d6ae8f20e91fe70146cbe/docs/ClassDiagram_LiftProgram.jpg)
